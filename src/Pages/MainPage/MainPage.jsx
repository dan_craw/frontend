import './MainPage.css';
import {MainPageView} from './MainPageView/MainPageView';
import {MainPageEducation} from './MainPageEducation/MainPageEducation';

export const MainPage = () => {
    return (
        <>
            <MainPageView />
            <MainPageEducation />
        </>
    )
}
