import { Link } from "react-router-dom";

import './MainPageEducation.css';

export const MainPageEducation = () => {
    return (
        <section className='mainPageEducation'>
            <div className='mainPageEducation-wrapper'>
                <Link
                    to={'higherEducation'}
                    className='mainPageEducation__link'
                >
                    <div className='mainPageEducation__icon'></div>
                    <h4 className='mainPageEducation__text'>Высшее образование</h4>
                </Link>
                <Link
                    to={'secondaryVocationalEducation'}
                    className='mainPageEducation__link'
                >
                    <div className='mainPageEducation__icon'></div>
                    <h4 className='mainPageEducation__text'>СПО</h4>
                </Link>
                <Link
                    to={'secondaryEducation'}
                    className='mainPageEducation__link'
                >
                    <div className='mainPageEducation__icon'></div>
                    <h4 className='mainPageEducation__text'>Среднее образование</h4>
                </Link>
            </div>
        </section>
    );
}