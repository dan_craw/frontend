import './MainPageView.css';
import { Link } from "react-router-dom";
// import { ReactComponent as MainImg } from '../../img/Vector.svg';
import { Btn } from "../../../CommonUI/Btn/Btn";
import mainImg from '../../../img/Vector.png';

export const MainPageView = () => {
    return (
        <section className='mainPageView'>
            <div className='mainPageView-wrapper'>
                <img src={mainImg} alt="" className={'mainPageImg'} />
                <div className='mainPageText'>
                    <h1 className='mainPageText__title'>StudentHub</h1>
                    <p className='mainPageText__text'>Идейные соображения высшего порядка, а также новая модель организационной деятельности влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач. Повседневная практика показывает, что реализация намеченных плановых заданий представляет собой интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных задач. Равным образом постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании дальнейших направлений развития.</p>

                    <p className='mainPageText__text'>Разнообразный и богатый опыт консультация с широким активом представляет собой 354 тс. интересный эксперимент проверки форм развития. Таким образом реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития.</p>

                    <p className='mainPageText__text'>Повседневная практика показывает, что постоянный количественный рост 947 и сфера нашей активности способствует подготовки и реализации направлений прогрессивного развития. Таким образом новая модель организационной деятельности влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что консультация с широким активом позволяет оценить значение модели развития. Задача организации, в особенности же рамки и место обучения кадров требуют от нас анализа систем массового участия. Равным образом укрепление и развитие структуры влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.
                    </p>
                    {/* <Btn text={'Разместить задание'} propClass={'postAssignment'} link={'postAssignment'} onClick={() => { }} /> */}
                    <Link to='/order-editor-step1' className='mainPageHeader__link'><Btn text={'Разместить задание'} propClass={'postAssignment'} /> </Link>
                </div>
            </div>
        </section >
    );
}