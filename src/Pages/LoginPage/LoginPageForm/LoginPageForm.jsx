import './LoginPageForm.css';
import { Link } from "react-router-dom";
import { LoginTitle } from '../LoginTitle/LoginTitle';
import { LoginText } from '../LoginText/LoginText';
import { Input } from '../../../CommonUI/Input/Input';
import { Btn } from '../../../CommonUI/Btn/Btn';
// import { LoginEntryLink } from '../LoginEntryLinks/LoginEntryLink';

export const LoginPageForm = () => {

   return (
      <section className='loginPageForm'>
         <LoginTitle text={'Вход'} propClass={'loginTitle loginPageForm__title'} />
         <form className='form form__login'>
            <div>
               <Input type={'text'} innerClass={'input-inner'} propClass={'form__form-input'} spanClass={'form-span'} spanText={'Email:'} />
               <Input type={'password'} innerClass={'input-inner'} propClass={'form__form-input'} spanClass={'form-span form-span2'} spanText={'Пароль:'} autocomplete="on" />
            </div>
            <LoginText text={'Забыли пароль ?'} propClass={'loginText loginText__text'} />
            <Btn text={'Войти'} propClass={'postAssignment'} />
            {/* <LoginEntryLink /> */}
            <Link to='/adduser'>
               <Btn text={'Регистрация'} propClass={'postAssignment'} />
            </Link>
         </form>
      </section>
   );
}