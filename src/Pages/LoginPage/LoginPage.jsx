import './LoginPage.css';
// import MainPageHeader from '../../components/MainPageHeader/MainPageHeader';
// import MainPageFooter from '../../components/MainPageFooter/MainPageFooter';
import {LoginPageForm} from './LoginPageForm/LoginPageForm';

export const LoginPage = () => {

   return (
      <div className={'loginPage'}>
         <LoginPageForm />
      </div>
   );
}