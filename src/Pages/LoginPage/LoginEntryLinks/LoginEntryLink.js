import './LoginLink.css';
import '../SocialNetworkLink/SocialNetworkLink.css';

import { nanoid } from "@reduxjs/toolkit";
import React from "react";

import { selectLinks } from './loginLinkSlice';
import { LoginText } from '../LoginText/LoginText';

export function LoginEntryLink() {
   return (
      <div className='social-link'>
         <LoginText text={'Войти через:'} propClass={'loginText'} />
         <div className={'loginNetworkInner'}>
            {selectLinks.map(item =>
               <a href={item.link} key={nanoid()} className={`loginNetworkLink ${item.iconClass}`}>
                  <img className={'loginNetworkIcon'} src={item.icon} alt={item.alt} />
               </a>
            )}
         </div>
      </div>
   );
}