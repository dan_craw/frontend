import { createSlice } from '@reduxjs/toolkit';

import vk from '../../img/icons/vk.svg';
import yandex from '../../img/icons/google.svg';// не нашел yandex (Найдете, замените)
import google from '../../img/icons/google.svg';

const initialState = [
   {
      iconClass: 'yandex',
      icon: yandex,
      link: 'yandex.com',
      alt: 'yandex',
   },
   {
      iconClass: 'google',
      icon: google,
      link: 'google.com',
      alt: 'google',
   },
   {
      iconClass: 'vk',
      icon: vk,
      link: 'vk.com',
      alt: 'vk',
   },
];


export const loginLinks = createSlice({
   name: 'links',
   initialState,
   reducers: {},
});

export const { } = loginLinks.actions;

export const selectLinks = initialState;

export default loginLinks.reducer;