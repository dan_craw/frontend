import './LoginText.css';

export const LoginText = ({ text, propClass }) => {

   return (
      <h3 className={propClass}>{text}</h3>
   );
}