import './LoginTitle.css';

export const LoginTitle = ({ text, propClass }) => {

   return (
      <h1 className={propClass}>{text}</h1>
   );
}