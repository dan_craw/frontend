import React from "react";
import classNames from "classnames";
import './button.scss'


export const Button = ({size,color,children,bRadius,fColor}) => {

    return (
        <button className={classNames("button", {
            'black' : fColor === 'black',
            'white' : fColor === 'white',
            'large' : size === 'large',
            'medium' : size === 'medium',
            'small' : size === 'small',
            'yellow' : color === 'yellow',
            'blue' : color === 'blue',
            'round' : bRadius === 40,
            'default' : bRadius === 15
        })}>
            {children}
        </button>
    )
}