import React from "react";
import {Link} from "react-router-dom";
import './profileOptions.scss'

export const ProfileOptions = () =>{
    return (
        <div className="profile-options">
            <div className="btnGroup">
                <Link>
                    <button>Ваши заказы</button>
                </Link>
                <Link>
                    <button>Ваши задания</button>
                </Link>
                <Link>
                    <button>Разместить заказ</button>
                </Link>
                <Link>
                    <button>Выбрать задание </button>
                </Link>
                <Link>
                    <button>Ваши отклики</button>
                </Link>
            </div>
        </div>
    )
}