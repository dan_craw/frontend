import React from "react";
import './profile.scss'
import {Button} from "../../components/Button/index";
import {Rating} from "../../components/Rating/Rating";
import {Link} from "react-router-dom";
import {ProfileOptions} from "../../components/ProfileOptions";

const Profile = ({}) => {

    const isPremium = true;

    const personSpecializations = ['математика','физика'];
    const personDescription = 'текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст';

    return(
        <>
            <div className="top-board"></div>
            <div className="profile-wrapper">
                <div className="container">
                    <section className="profile-view">
                        <div className="profile-view--avatar">
                            <img className={isPremium && "premium"} src="https://place-hold.it/300" alt="avatar"/>
                            {isPremium && <div className="premium-badge">Premium</div>}
                        </div>
                        <div className="profile-view--about">
                            <h3 className="profile-view--about-name">Андрей Петров </h3>
                            <div className="profile-view--about-rating">
                                <h4 className="profile-view--about-text">Рейтинг</h4>
                                <Rating rating={4} isEditable={false}/>
                            </div>
                            <Link to={'/reviews'}><h4 className="profile-view--about-text">Отзывы</h4></Link>
                            <div className="profile-view--about-specializations">
                                <h4 className="profile-view--about-text" >
                                    Специализация:
                                </h4>
                                {personSpecializations.map((item,index) => {
                                    if(index === 0){
                                        item = ` ${item}, `
                                    }
                                    else if(index ===personSpecializations.length-1){
                                        item = ` ${item}.`
                                    }
                                    else {
                                        item = `${item} `
                                    }
                                    return (
                                        <h5 key={`${index}+${item}`}>{item} </h5>
                                    )
                                })}
                            </div>
                        </div>
                    </section>
                    <h4 className="profile-view--about-text about">О себе: <span>{personDescription}</span></h4>
                    <div className="account-btnGroup">
                        <Button bRadius={15} color="blue" size="small" fColor="white">Редактировать</Button>
                        <Button bRadius={15} color="blue" size="small" fColor="white">Купить премиум</Button>
                    </div>
                    <ProfileOptions/>
                    <h2>компонент профиля</h2>
                </div>
            </div>
        </>
    )
}

export default Profile;