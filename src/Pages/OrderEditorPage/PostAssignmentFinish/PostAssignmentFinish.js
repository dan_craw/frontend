import { useSelector, useDispatch } from "react-redux";
import { addedDataFromStep3 } from "../../../store/currentOrderSlice";
import { Btn } from "../../../CommonUI/Btn/Btn";

export const PostAssignmentFinish = () => {
    const dispatch = useDispatch();
    const {
        description,
        direction,
        section,
        secondDescription,
        budget,
        deadline,
        safeDeal
    } = useSelector(state => state.currentOrder);
    console.log(direction);
    return (
        <>
            <h2
                className={'postAssignmentFinish-h2'}
            >
                Твой заказ
            </h2>
            <p className="memo postAssignmentFinish-memo">
                <h3 className="postAssignmentFinish-h3">
                    Направление:
                    <span className="postAssignmentFinish-h3_content">
                        {direction}
                    </span>
                </h3>
                <h3 className="postAssignmentFinish-h3">
                    Раздел:
                    <span className="postAssignmentFinish-h3_content">
                        {section}
                    </span>
                </h3>
                <h3 className="postAssignmentFinish-h3">
                    Описание:
                    <span className="postAssignmentFinish-h3_content">
                        {description}
                    </span>
                </h3>
                <h3 className="postAssignmentFinish-h3">
                    Подробное описание:
                    <span className="postAssignmentFinish-h3_content">
                        {secondDescription}
                    </span>
                </h3>
                <span className="postAssignmentFinish-wrapper">
                    <h3 className="postAssignmentFinish-h3">
                        Бюджет:
                        <span className="postAssignmentFinish-h3_content">
                            {budget}
                        </span>
                    </h3>
                    <h3 className="postAssignmentFinish-h3">
                        Срок:
                        <span className="postAssignmentFinish-h3_content">
                            {deadline}
                        </span>
                    </h3>
                </span>
                <h3 className="postAssignmentFinish-h3">
                    Безопасная сделка <span className="postAssignmentFinish-h3_content">{safeDeal ? 'да' : 'нет'}</span>
                </h3>
            </p>
            <Btn
                onClick={() => { }}
                text={'Опубликовать заказ'}
                propClass={'postAssignment postAssignmentFinishBtn'}
                className={'postAssignment-next'}
            />
        </>
    );
};