import '../OrderEditorPage.css';
import { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import { addedDataFromStep1 } from "../../../store/currentOrderSlice";
import { Btn } from "../../../CommonUI/Btn/Btn";
import { Textarea } from "../../../CommonUI/Textarea/Textarea";
import correct from "../../../img/icons/correct.svg";


export const PostAssignmentStep1 = () => {
    const dispatch = useDispatch();
    const [description, setDescription] = useState('')
    const [edu, setEdu] = useState('');
    const [subject, setSubject] = useState('');
    const [showDropdown, setShowDropdown] = useState(false);

    return (
        <div className={'postAssignmentWizard'}>
            <h2 className='postAssignmentWizard-title'>
                Редактор заказа
            </h2>
            <h3 className={'postAssignmentWizard-h3 postAssignmentWizard-h3__el1'}>Что сделать?</h3>
            <Textarea
                onChange={(e) => {
                    setDescription(e.target.value);
                }}
                placeholder={'краткое описание'}
            />
            <h3 className={'postAssignmentWizard-h3 postAssignmentWizard-h3__el2'}>Выбрать направление</h3>
            <p
                className="select"
                onClick={() => {
                    setEdu('Высшее образование');
                }}
            >Высшее образование {edu === 'Высшее образование' && <img
                src={correct}
                alt=""
                className={'postAssignmentWizard-icon'}
            />}</p>
            <p
                className="select"
                onClick={() => {
                    setEdu('СПО');
                }}
            >СПО {edu === 'СПО' && <img
                src={correct}
                alt=""
                className={'postAssignmentWizard-icon'} />}</p>
            <p
                className="select"
                onClick={() => {
                    setEdu('Школа');
                }}
            >Школа {edu === 'Школа' && <img
                src={correct}
                alt=""
                className={'postAssignmentWizard-icon'} />}</p>
            <h3 className="postAssignmentWizard-h3 postAssignmentWizard-h3__el3">Выбрать раздел</h3>
            <p
                className="select select-subject"
                onClick={() => {
                    setShowDropdown(!showDropdown);
                }}
            >
                IT
            </p>
            {showDropdown &&
                <>
                    <p
                        className="select select-dropdown"
                        onClick={() => {
                            setSubject('Веб');
                        }}
                    >
                        Веб {subject === 'Веб' && <img
                            src={correct}
                            alt=""
                            className={'postAssignmentWizard-icon'} />}
                    </p>
                    <p
                        className="select select-dropdown"
                        onClick={() => {
                            setSubject('Компьютерная графика');
                        }}
                    >
                        Компьютерная графика {subject === 'Компьютерная графика' && <img
                            src={correct}
                            alt=""
                            className={'postAssignmentWizard-icon'} />}
                    </p>
                </>
            }
            <p
                className="select select-subject"
                onClick={() => {
                    setSubject('Математика');
                }}
            >Математика {subject === 'Математика' && <img
                src={correct}
                alt=""
                className={'postAssignmentWizard-icon'} />}
            </p>
            <p
                className="select select-subject select-last"
                onClick={() => {
                    setSubject('Английский');
                }}
            >Английский {subject === 'Английский' && <img
                src={correct}
                alt=""
                className={'postAssignmentWizard-icon'} />}
            </p>
            <Link to='/order-editor-step2'>
                <Btn
                    onClick={description && edu && subject ? () => dispatch(addedDataFromStep1({
                        description,
                        direction: edu,
                        section: subject
                    })) : null}
                    text={'Продолжить'}
                    propClass={'postAssignment'}
                    link={'/postAssignment/step2'}
                    className={'postAssignment-next'}
                />
            </Link>

        </div>
    );
}