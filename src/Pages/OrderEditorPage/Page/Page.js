import '../MainPage/MainPage.css'
import {MainPage} from "../MainPage/MainPage";
import {MainPageHeader} from "../MainPageHeader/MainPageHeader";
import {MainPageFooter} from "../MainPageFooter/MainPageFooter";
import {Switch, Route} from "react-router-dom";
import {PostAssignmentWizard} from "../PostAssignmentWizard/PostAssignmentWizard";

export const Page = () => {
    
    return (
        <div className={'mainPage'}>
            <MainPageHeader />
            <Switch>
                <Route exact path={'/'} component={MainPage} />
                <Route path={'/postAssignment'} component={PostAssignmentWizard} />
            </Switch>
            <MainPageFooter />
        </div>

    );
};