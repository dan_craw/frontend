import {useState} from "react";
import {Switch, Redirect, Route} from "react-router-dom";

import './PostAssignmentWizard.css';
import {Textarea} from "../Textarea/Textarea";
import correct from '../../img/icons/correct.svg';
import {Btn} from "../Btn/Btn";
import {PostAssignmentStep1} from "../PostAssignmentStep1/PostAssignmentStep1";
import {PostAssignmentStep2} from "../PostAssignmentStep2/PostAssignmentStep2";
import {PostAssignmentStep3} from "../PostAssignmentStep3/PostAssignmentStep3";
import {PostAssignmentFinish} from "../PostAssignmentFinish/PostAssignmentFinish";

export const PostAssignmentWizard = () => {

    return (
        <div className={'postAssignmentWizard'}>
            <Switch>
                <Route exact path={'/postAssignment/step1'} component={PostAssignmentStep1} />
                <Route exact path={'/postAssignment/step2'} component={PostAssignmentStep2} />
                <Route exact path={'/postAssignment/step3'} component={PostAssignmentStep3} />
                <Route exact path={'/postAssignment/finish'} component={PostAssignmentFinish} />
                <Redirect to={'/postAssignment/step1'} component={PostAssignmentStep1} />
            </Switch>
        </div>
    )
}