import '../OrderEditorPage.css';
import { Link } from "react-router-dom";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addedDataFromStep3 } from "../../../store/currentOrderSlice";
import { Btn } from "../../../CommonUI/Btn/Btn";
import { Input } from "../../../CommonUI/Input/Input";

export const PostAssignmentStep3 = () => {
    const dispatch = useDispatch();

    const [budget, setBudget] = useState('');
    const [deadline, setDeadline] = useState('');
    const [safeDeal, setSafeDeal] = useState(false);

    return (
        <div className={'postAssignmentWizard'}>
            <h2 className="PostAssignmentStep3-budgetsАndDeadlines">Бюджет и срок</h2>
            <p className="memo">Памятка</p>
            <Input
                value={budget}
                onChange={(e) => setBudget(e.target.value)}
                id={'PostAssignmentStep3-budget'}
                label={'Бюджет'}
                placeholder={'мин. 100 руб'}
            />
            <Input
                value={deadline}
                onChange={(e) => setDeadline(e.target.value)}
                id={'PostAssignmentStep3-deadline'}
                label={'Срок'}
                placeholder={'мин. 1 день'}
                addSmth={'calendar'}
            />
            <input
                onClick={() => {
                    setSafeDeal(!safeDeal)
                }}
                value={safeDeal}
                id={'PostAssignmentStep3-safeDeal'}
                type="checkbox"
                className="PostAssignmentStep3-safeDeal-input"
            />
            <label htmlFor={'PostAssignmentStep3-safeDeal'}>Безопасная сделка</label>
            <Link to='/order-editor-finish'>
                <Btn
                    onClick={budget && deadline ? () => dispatch(addedDataFromStep3({ budget, deadline, safeDeal })) : null}
                    text={'Продолжить'}
                    propClass={'postAssignment'}
                    link={'/postAssignment/finish'}
                    className={'postAssignment-next'}
                />
            </Link>
        </div>
    );
};