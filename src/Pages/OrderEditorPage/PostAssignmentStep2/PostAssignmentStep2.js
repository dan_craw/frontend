import { Link } from "react-router-dom";
import '../OrderEditorPage.css';
import { Textarea } from "../../../CommonUI/Textarea/Textarea";
import { Btn } from "../../../CommonUI/Btn/Btn";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addedDataFromStep2 } from "../../../store/currentOrderSlice";
import upload from '../../../img/icons/upload.svg';
import close from '../../../img/icons/close.svg';

export const PostAssignmentStep2 =() => {
    const dispatch = useDispatch();
    const [secondDescription, setSecondDescription] = useState('');

    return (
        <div className={'postAssignmentWizard'}>
            <h2 className={'postAssignmentWizard-title_step2'}>Подробное описание</h2>
            <p className="memo">Памятка</p>
            <h3 className="postAssignmentWizard-title_step2-h3">Описание</h3>
            <Textarea
                value={secondDescription}
                placeholder={'Введите текст'}
                onChange={(e) => setSecondDescription(e.target.value)}
            />
            <h3 className="postAssignmentWizard-title_step2-h3-upload">Загрузить файл</h3>
            <div className="postAssignmentWizard-upload">
                <img src={upload} alt="Загрузить" />
            </div>
            <div className="addedFile">
                добавленный файл
                <img src={close} alt="close" />
            </div>
            <Link to='/order-editor-step3'>
                <Btn
                    onClick={secondDescription ? () => dispatch(addedDataFromStep2({ secondDescription })) : null}
                    text={'Продолжить'}
                    propClass={'postAssignment'}
                    link={'/postAssignment/step3'}
                    className={'postAssignment-next'}
                />
            </Link>
        </div>
    );
};