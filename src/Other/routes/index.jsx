import React from 'react';
import { Route, Router } from 'react-router';
import history from '../utils/routing';
import LoginPage from '../pages/LoginPage/LoginPage';
import MainPage from '../pages/MainPage/MainPage';
import Profile from '../pages/Profile';
import { AddUserPage } from '../components/AddUser/AddUserPage/AddUserPage';
import PostAssignmentStep1 from '../components/PostAssignmentStep1/PostAssignmentStep1';
import PostAssignmentStep2 from '../components/PostAssignmentStep2/PostAssignmentStep2';
import PostAssignmentStep3 from '../components/PostAssignmentStep3/PostAssignmentStep3';
import PostAssignmentFinish from '../components/PostAssignmentFinish/PostAssignmentFinish';

const routes = [
  {
    id: 'main',
    path: '/',
    exact: true,
    component: MainPage,
  },
  {
    id: 'PostAssignmentStep2',
    path: '/postAssignment/step2',
    exact: true,
    component: PostAssignmentStep2,
  },
  {
    id: 'PostAssignmentStep3',
    path: '/postAssignment/step3',
    exact: true,
    component: PostAssignmentStep3,
  },
  {
    id: 'PostAssignmentFinish',
    path: '/postAssignment/finish',
    exact: true,
    component: PostAssignmentFinish,
  },
  {
    id:'login',
    path:'/login',
    exact:true,
    component:LoginPage
  },
  {
    id:'postAssignment',
    path:'/postAssignment',
    exact:true,
    component:PostAssignmentStep1
  },
  {
    id:'profile',
    path:'/profile',
    exact:true,
    component:Profile
  },
  {
    id:'addUser',
    path:"/adduser",
    exact:true,
    component:AddUserPage
  }
];

const RouterSwitch = () => {
  return (
    <Router history={history}>
      {routes.map((e) => {
        const { id, ...props } = e;
        return <Route key={id} {...props} />;
      })}
    </Router>
  );
};

export default RouterSwitch;
