import { Outlet } from "react-router-dom";
import {PageHeader} from "../../../CommonUI/Header/Header";
import {PageFooter} from '../../../CommonUI/Footer/Footer';

export const Layout = () => {
   return (
      <>
         <PageHeader />
         <Outlet />
         <PageFooter />
      </>
   )
}
