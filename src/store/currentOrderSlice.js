import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
    description: '',
    direction: '',
    section: '',
    secondDescription: '',
    budget: '',
    deadline: '',
    safeDeal: false,
}

export const currentOrderSlice = createSlice({
    name: 'currentOrder',
    initialState,
    reducers: {
        addedDataFromStep1: (state, action) => {
                let {description, direction, section} = action.payload;
                state.description = description;
                state.direction = direction;
                state.section = section;
            },
        addedDataFromStep2: (state, action) => {
            state.secondDescription = action.payload.secondDescription;
        },
        addedDataFromStep3: (state, action) => {
            let {budget, deadline, safeDeal} = action.payload;
            state.budget = budget;
            state.deadline = deadline;
            state.safeDeal = safeDeal;
        }
    }
});

export const {addedDataFromStep1, addedDataFromStep2, addedDataFromStep3} = currentOrderSlice.actions;

export default currentOrderSlice.reducer;