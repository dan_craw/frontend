import { configureStore } from '@reduxjs/toolkit';
import firstReducer from "../store/firstSlice";
import currentOrderSlice from './currentOrderSlice';
import socialNetworkSlice from './socialNetworkSlice';

//=== Стор =====

const store = configureStore({
   reducer: {
      currentOrder: currentOrderSlice,
      socialNetworks: socialNetworkSlice,
      guest: firstReducer,
   },
});
export default store;