import { createSlice } from "@reduxjs/toolkit";

//==== Редюсер =====

export const firstSlice = createSlice({
   name: 'guest',
   initialState: {},
   reducers: {
   }
});


export const selectGuestArr = state => state.guest.guestList;
export default firstSlice.reducer;