import './Footer.css';
import {SocialNetworks} from '../Footer/SocialNetworks/SocialNetworks';
import {FooterInfo} from '../Footer/FooterInfo/FooterInfo';

export const PageFooter = () => {
    return (
        <footer className={'mainPageFooter'}>
            <div className="mainPageFooter-wrapper">
                <FooterInfo />
                {/* <SocialNetworks /> */}
            </div>
        </footer>
    );
}