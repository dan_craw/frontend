import './SocialNetworkLink.css';

export const SocialNetworkLink = ({ iconClass, icon, link, alt }) => {
    return (
        <a href={link} className={`socialNetworkLink ${iconClass}`}>
            <img src={icon} className={'socialNetworkIcon'} alt={alt} />
        </a>
    );
}