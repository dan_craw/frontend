import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import vk from '../../img/icons/vk.svg';
import inst from '../../img/icons/inst.svg';
import youtube from '../../img/icons/youtube.svg';
import google from '../../img/icons/google.svg';
import whatsapp from '../../img/icons/whatsapp.svg';
import telegram from '../../img/icons/telegram.svg';

const initialState = [
    {
        iconClass: 'vk',
        icon: vk,
        link: 'vk.com',
        alt: 'vk',
    },
    {
        iconClass: 'inst',
        icon: inst,
        link: 'instagram.com',
        alt: 'instagram',
    },
    {
        iconClass: 'youtube',
        icon: youtube,
        link: 'youtube.com',
        alt: 'youtube',
    },
    {
        iconClass: 'google',
        icon: google,
        link: 'google.com',
        alt: 'google',
    },
    {
        iconClass: 'whatsapp',
        icon: whatsapp,
        link: 'https://web.whatsapp.com/',
        alt: 'whatsapp',
    },
    {
        iconClass: 'telegram',
        icon: telegram,
        link: 'https://web.telegram.org/z/',
        alt: 'telegram',
    }
];

export const socialNetworkSlice = createSlice({
    name: 'socialNetworks',
    initialState,
    reducers: {},
});

export const {} = socialNetworkSlice.actions;

export const selectSocialNetworks = state => state.socialNetworks;

export default socialNetworkSlice.reducer;