import { useSelector } from 'react-redux';
import {SocialNetworkLink} from '../SocialNetworks/SocialNetworkLink/SocialNetworkLink';
import { nanoid } from "@reduxjs/toolkit";

import './SocialNetworks.css';
import selectSocialNetworks from '../../../store/socialNetworkSlice';

export const SocialNetworks = () => {
    const socialNetworks = useSelector(selectSocialNetworks);

    return (
        <div className={'socialNetworks'}>
            <div className='socialNetworks-wrapper'>
                {socialNetworks.map(net => <SocialNetworkLink
                    icon={net.icon}
                    link={net.link}
                    iconClass={net.iconClass}
                    alt={net.alt}
                    key={nanoid()}
                />)}
            </div>
        </div>
    );
}