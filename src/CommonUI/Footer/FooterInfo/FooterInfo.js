import './FooterInfo.css';
import { Link } from 'react-router-dom';
import logo from '../../../img/icons/logo.svg';

export const FooterInfo = () => {

    return (
        <div className={'footerInfo'}>
            <img

                src={logo}
                alt='Studenthub'
                className={'footerInfo__logo'}
            />
            <div className='footerInfo-lists'>
                <ul className={'footerInfo-lists__list'}>
                    <li>
                        <Link
                            to={'/aboutStudentHub'}
                            className={'footerInfo__link'}
                        >
                            О StudentHub
                        </Link>
                    </li>

                    <li>
                        <Link
                            to={'/aboutProject'}
                            className={'footerInfo__link'}
                        >
                            О проекте
                        </Link>
                    </li>
                    <li>
                        <Link
                            to={'/rules'}
                            className={'footerInfo__link'}
                        >
                            Правила
                        </Link>
                    </li>
                </ul>
                <ul className={'footerInfo-lists__list'}>
                    <li>
                        <Link
                            to={'/useful'}
                            className={'footerInfo__link'}
                        >
                            Полезное
                        </Link>
                    </li>
                    <li>
                        <Link
                            to={'/blog'}
                            className={'footerInfo__link'}
                        >
                            Блог
                        </Link>
                    </li>
                    <li>
                        <Link
                            to={'/cooperation'}
                            className={'footerInfo__link'}
                        >
                            Сотрудничество
                        </Link>
                    </li>
                </ul>
                <ul className={'footerInfo-lists__list'}>
                    <li>
                        <Link
                            to={'/help'}
                            className={'footerInfo__link'}
                        >
                            Помощь
                        </Link>
                    </li>
                    <li>
                        <Link
                            to={'/support'}
                            className={'footerInfo__link'}
                        >
                            Служба поддержки
                        </Link>
                    </li>
                    <li>
                        <Link
                            to={'/faq'}
                            className={'footerInfo__link'}
                        >
                            Часто задаваемые вопросы
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    );
}