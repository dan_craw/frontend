import './Input.css';
import calendar from '../../img/icons/calendar.svg';

export const Input = ({id, label, onChange, value, placeholder, addSmth}) => {
    let component = label
        ? (<label className={'input-wrapper'}>
            <h4 className={'input-title'}>{label}</h4>
            <input
                value={value}
                onChange={onChange}
                className='input'
                id={id}
                placeholder={placeholder}
            />
            {addSmth && <img
                src={calendar}
                alt={'Выберите дату'}
                className={'input-calendar'}
            />}
        </label>)
        : (<input
            className={'input'}
            id={id}
        />)
    return (component);
}