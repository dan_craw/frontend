import './Textarea.css';

export const Textarea = ({placeholder, onChange}) => {
    return (
        <textarea name="" id="" cols="30" rows="10"
                  onChange={onChange}
                  placeholder={placeholder}
                  className="textarea"
        >

        </textarea>
    );
};