// import MobileNavigation from "./MobileNavigation";
import Navigation from "./Navigation";

function NavBar() {
    return (
        <div className='navBar'>
            {/* <MobileNavigation /> */}
            <Navigation />
        </div>
    );
};
export default NavBar;