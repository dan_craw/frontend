import './MainPageHeader.css';
import logoWithoutText from '../../img/icons/logoWithoutText.svg';
import { Link } from "react-router-dom";
import { Btn } from "../Btn/Btn";
// import NavBar from "./NavBar";

export const PageHeader = () => {

    return (
        <header className='mainPageHeader'>
            <div className='mainPageHeader-wrapper'>
                <div className="mainPageHeaderCol1">
                    <Link to={'/'} className='mainPageHeader__link'>
                        <img src={logoWithoutText} alt="" />
                        <h3 className={'mainPageHeader__link-text'}>StudentHub</h3>
                    </Link>
                    {/* <NavBar /> */}
                </div>
                <Link to='/login'>
                    <Btn text={'Вход/Регистрация'} propClass={'loginBtn'} />
                </Link>
            </div>
        </header >
    );
}