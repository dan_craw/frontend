import {NavLinks} from './NavLinks';
import {CgMenuRound} from 'react-icons/cg';
import {CgCloseO} from 'react-icons/cg';
import {useState} from 'react';

const MobileNavigation = () => {
    const [open, setOpen] = useState(false);
    const hamburgerIcon = <CgMenuRound
        className='hamburger'
        size='80px'
        color='green'
        onClick={() => {
            setOpen(!open);
        }}
    />;
    const closeIcon = <CgCloseO
        className='hamburger'
        size='80px'
        color='green'
        onClick={() => {
            setOpen(!open);
        }}
    />

    const closeMobileMenu = () => setOpen(false);

    return (
        <nav className='mobileNavigation'>
            {open ? closeIcon : hamburgerIcon}
            {open && <NavLinks isMobile={true} closeMobileMenu={closeMobileMenu}/>}
        </nav>
    );
}

export default MobileNavigation;