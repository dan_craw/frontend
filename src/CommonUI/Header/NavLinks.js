import { useState } from 'react';
import { motion } from 'framer-motion';
import { Link } from "react-router-dom";


function NavLinks(props) {
    const [openDropdown1, setOpenDropDown1] = useState(false);
    const [openDropdown2, setOpenDropDown2] = useState(false);
    const animateFrom = { opacity: 0, y: -40 };
    const animateTo = { opacity: 1, y: 0 };
    return (
        <ul className={'mainPageHeader-Nav'}>
            <motion.li
                initial={animateFrom}
                animate={animateTo}
                transition={{ delay: 0.05 }}
                onClick={() => props.isMobile && props.closeMobileMenu()}
                className="mainPageHeader-btns"
            >
                Актуальные задания
            </motion.li>
            <motion.li
                initial={animateFrom}
                animate={animateTo}
                transition={{ delay: 0.1 }}
                // onClick={() => {
                //     if(props.isMobile) props.closeMobileMenu();
                //     else setOpenDropDown1(!openDropdown1)
                // }}
                onClick={() => {
                    if (props.isMobile) {
                        setOpenDropDown1(!openDropdown1);
                    }
                    else setOpenDropDown1(!openDropdown1)
                }}
                className={props.isMobile ? "mainPageHeader-btns mainPageHeader-btns_active" : "mainPageHeader-btns"}
            >
                Разместить задание
                {openDropdown1 && !props.isMobile && <ul className="mainPageDropdown"><Dropdown1 /></ul>}
                {openDropdown1 && props.isMobile && <Dropdown1 isMobile={true} />}
            </motion.li>
            <motion.li
                initial={animateFrom}
                animate={animateTo}
                transition={{ delay: 0.15 }}
                onClick={() => {
                    if (props.isMobile) {
                        setOpenDropDown2(!openDropdown2);
                    }
                    else setOpenDropDown2(!openDropdown2)
                }}
                className="mainPageHeader-btns"

            >
                Ещё
                {openDropdown2 && props.isMobile && <Dropdown2 isMobile={true} />}
                {openDropdown2 && !props.isMobile && <ul className="mainPageDropdown">
                    <Dropdown2 />
                </ul>}
            </motion.li>

        </ul>
    );
}
export default NavLinks;

const Dropdown1 = ({ isMobile }) => {
    return (
        <>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}><Link>Высшее образование</Link></li>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}><Link>СПО</Link></li>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}><Link>Среднее</Link></li>
        </>
    )
}

const Dropdown2 = ({ isMobile }) => {
    return (
        <>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}>
                <Link>Безопасная сделка</Link>
            </li>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}>
                <Link>Премиум аккаунт</Link>
            </li>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}>
                <Link>Тех. поддержка</Link>
            </li>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}>
                <Link>О нас</Link>
            </li>
            <li className={isMobile ? "mainPageHeader-btns mainPageHeader-btns_dropDown" : ""}>
                <Link>Отзывы и предложения</Link>
            </li>
        </>
    );
}
