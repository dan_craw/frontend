import {Link} from 'react-router-dom';

import './Btn.css';

export const Btn = ({text, propClass, link, onClick}) => {
    console.log(onClick)

    return (
        <button className={propClass} onClick={onClick}>
            {link && onClick
                ? <Link to={link} >{text}</Link>
                : text
            }
        </button>
    );
}