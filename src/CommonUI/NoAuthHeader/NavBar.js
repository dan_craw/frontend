import MobileNavigation from "./MobileNavigation";
import Navigation from "./Navigation";

export const NavBar = () => {
    return (
        <div className='navBar'>
            <MobileNavigation />
            <Navigation />
        </div>
    );
};