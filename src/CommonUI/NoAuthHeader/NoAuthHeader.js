import {Link} from "react-router-dom";

import './MainPageHeader.css';
import logoWithoutText from '../../img/icons/logoWithoutText.svg';
import {Btn} from "../Btn/Btn";
import {NavBar} from "./NavBar";

export const MainPageHeader = () => {

    return (
        <header className='mainPageHeader'>
            <div className='mainPageHeader-wrapper'>
                <div className="mainPageHeaderCol1">
                    <Link to={'/'} className='mainPageHeader__link'>
                        <img
                            className={'mainPageHeader__img'}
                            src={logoWithoutText}
                            alt=""/>
                        <h3 className={'mainPageHeader__link-text'}>StudentHub</h3>
                    </Link>
                    <NavBar />
                </div>
                <Btn text={'Вход/Регистрация'} propClass={'loginBtn'} />
            </div>
        </header>
    );
}