import './App.css';
import React from 'react';
import { Routes, Route } from 'react-router-dom';

import {LoginPage} from './Pages/LoginPage/LoginPage';
import {MainPage} from './Pages/MainPage/MainPage';
import {PostAssignmentStep1} from './Pages/OrderEditorPage/PostAssignmentStep1/PostAssignmentStep1';
import {PostAssignmentStep2} from './Pages/OrderEditorPage/PostAssignmentStep2/PostAssignmentStep2';
import {PostAssignmentStep3} from './Pages/OrderEditorPage/PostAssignmentStep3/PostAssignmentStep3';
import {PostAssignmentFinish} from './Pages/OrderEditorPage/PostAssignmentFinish/PostAssignmentFinish'
import {Layout} from './Other/routes/Layout/Layout';


export const App = () => {
  return (
    <div className={'mainPage'}>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index path="/" element={<MainPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/order-editor-step1" element={<PostAssignmentStep1 />} />
          <Route path="/order-editor-step2" element={<PostAssignmentStep2 />} />
          <Route path="/order-editor-step3" element={<PostAssignmentStep3 />} />
          <Route path="/order-editor-finish" element={<PostAssignmentFinish />} />
        </Route>
      </Routes>
    </div>
  );
} 

